import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContatosEditarComponent } from './contatos-editar.component';

describe('ContatosEditarComponent', () => {
  let component: ContatosEditarComponent;
  let fixture: ComponentFixture<ContatosEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContatosEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContatosEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
